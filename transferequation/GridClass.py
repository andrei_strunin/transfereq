from AxisClass import Axis

class Grid3d():
    """3 dimensional grid"""

    def __init__ (self, x:Axis, y:Axis):
        self.x = x
        self.y = y


    def true_val(self, i:int, j:int, f:callable):
        return f(self.x.get_point(i), self.y.get_point(j))


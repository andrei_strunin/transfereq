import copy
from AxisClass import Axis
from GridClass import Grid3d

class Solver():
    """Solver of transfer equation"""
  
    def __init__(self, grid:Grid3d, init_cond:callable, left_border:callable, right_border:callable, c):
        self.grid = grid
        self.init_cond = init_cond
        self.left_border = left_border
        self.right_border = right_border
        self.c = c


    def true_solve(self, x, t):
       return self.init_cond(x - self.c*t)

    #This prints true solve of transfer eq
    def print_true_solve(self):
        f = open('truesolve.txt', 'w')

        for j in range(self.grid.y.n):
            for i in range(self.grid.x.n):
                f.write(str(self.grid.true_val(i, j, self.true_solve)) + ' ')
            f.write('\n')

    #This prints solve of laks scheme of transfer eq (put in true if u want to check nevyazka)
    def print_laks_solve(self, check_nevyazka:bool = False):

        if (check_nevyazka):
            s = 1
            f = open('laksnev.txt', 'w')
        else:
            s = 0
            f = open('laks.txt', 'w')

        nx = self.grid.x.n
        ny = self.grid.y.n
        dx = self.grid.x.get_h()
        dy = self.grid.y.get_h()

        future = [None for i in range(nx)]
        past = [self.init_cond(self.grid.x.get_point(i)) for i in range(nx)]

        for i in range(nx):
            f.write(str(past[i] - s * self.grid.true_val(i, 0, self.true_solve)) + ' ')
        f.write('\n')

        for j in range(1, ny):
            future[0] = self.left_border(self.grid.y.get_point(j))
            f.write(str(future[0] - s * self.grid.true_val(0, j, self.true_solve)) + ' ')
            future[nx - 1] = self.right_border(self.grid.y.get_point(j))
            for i in range(1, nx - 1):
                future[i] = -self.c*dy / (2*dx) * (past[i + 1] - past[i - 1]) + (past[i + 1] + past[i - 1]) / 2
                f.write(str(future[i] - s * self.grid.true_val(i, j, self.true_solve)) + ' ')
            f.write(str(future[nx - 1] - s * self.grid.true_val(nx - 1, j, self.true_solve)) + '\n')
            past = copy.deepcopy(future)

    #This prints solve of cheharda scheme of transfer eq (put in true if u want to check nevyazka)
    def print_cheharda_solve(self, check_nevyazka:bool = False):

        if (check_nevyazka):
            s = 1
            f = open('chehardanev.txt', 'w')
        else:
            s = 0
            f = open('cheharda.txt', 'w')

        nx = self.grid.x.n
        ny = self.grid.y.n
        dx = self.grid.x.get_h()
        dy = self.grid.y.get_h()

        future = [None for i in range(nx)]
        past = [[self.init_cond(self.grid.x.get_point(i)) for i in range(nx)] , [self.grid.true_val(i, 1, self.true_solve) for i in range(nx)]]

        for j in range(2):
            for i in range(nx):
                f.write(str(past[j][i] - s * self.grid.true_val(i, j, self.true_solve)) + ' ')
            f.write('\n')

        for j in range(2, ny):
            future[0] = self.left_border(self.grid.y.get_point(j))
            f.write(str(future[0] - s * self.grid.true_val(0, j, self.true_solve)) + ' ')
            future[nx - 1] = self.right_border(self.grid.y.get_point(j))
            for i in range(1, nx - 1):
                future[i] = past[0][i] - self.c * (dy / dx) * (past[1][i + 1] - past[1][i - 1])
                f.write(str(future[i] - s * self.grid.true_val(i, j, self.true_solve)) + ' ')
            f.write(str(future[nx - 1] - s * self.grid.true_val(nx - 1, j, self.true_solve)) + '\n')
            past[0] = copy.deepcopy(past[1])
            past[1] = copy.deepcopy(future)

    #This prints solve of predictor-corrector scheme of transfer eq (put in true if u want to check nevyazka)
    def print_predcor_solve(self, check_nevyazka:bool = False):

        if (check_nevyazka):
            s = 1
            f = open('predcornev.txt', 'w')
        else:
            s = 0
            f = open('predcor.txt', 'w')

        dx = self.grid.x.get_h()
        dy = self.grid.y.get_h()

        x1 = Axis(self.grid.x.A, self.grid.x.B, 2 * self.grid.x.n - 1)
        y1 = Axis(self.grid.y.A, self.grid.y.B, 2 * self.grid.y.n - 1)
        grid1 = Grid3d(x1, y1)
        nx1 = grid1.x.n
        ny1 = grid1.y.n

        future = [None for i in range(nx1)]
        present = [None for i in range(nx1)]
        past = [self.init_cond(x1.get_point(i)) for i in range(nx1)]
        for i in range(nx1):
            if (i % 2 == 0):
                f.write(str(past[i] - s * grid1.true_val(i, 0, self.true_solve)) + ' ')
        f.write('\n')

        for j in range(1,ny1):
            #predictor
            if (j % 2 == 1):
                present[0] = self.left_border(y1.get_point(j))
                present[nx1 - 1] = self.right_border(y1.get_point(j))
                for i in range(1, nx1 - 1):
                    present[i] = (-self.c*dy / (2*dx)) * (past[i + 1] - past[i - 1]) + (past[i + 1] + past[i - 1]) / 2
            #corrector
            else:
                future[0] = self.left_border(y1.get_point(j))
                f.write(str(future[0] - s * grid1.true_val(0, j, self.true_solve)) + ' ')
                future[nx1 - 1] = self.right_border(y1.get_point(j))
                for i in range (2, nx1 - 2, 2):
                    #if (i % 2 == 0):
                    future[i] = past[i] - self.c * (dy / dx) * (present[i + 1] - present[i - 1])
                    f.write(str(future[i] - s * grid1.true_val(i, j, self.true_solve)) + ' ')
                f.write(str(future[nx1 - 1] - s * grid1.true_val(nx1 - 1, j, self.true_solve)) + '\n')
                past = copy.deepcopy(present)
                present = copy.deepcopy(future)
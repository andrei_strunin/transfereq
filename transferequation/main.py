import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from AxisClass import Axis
from GridClass import Grid3d
from SolverClass import Solver

y = Axis(0, 10, 100)
x = Axis(0, math.pi, 100)
grid = Grid3d(x, y)
c = float(0.01)

if (c * y.get_h() / x.get_h() <= 1):
    print('C-F-L condition respected')
else:
    print('C-F-L condition disrespected')

def init_cond(x):
    return math.sin(x) + 2*math.sin(2*x)

def left_border(t):
    return init_cond(-c*t);

def right_border(t):
    return init_cond(math.pi - c*t);

solve = Solver(grid, init_cond, left_border, right_border, c)
solve.print_laks_solve(True)
solve.print_true_solve()
solve.print_cheharda_solve(True)
solve.print_cheharda_solve()
solve.print_predcor_solve()
solve.print_predcor_solve(True)

"""
fig=plt.figure

for i in range (x.n):
    plt.scatter(x.get_point(i), solve[i])
    init_cond(-t)
plt.show()
"""
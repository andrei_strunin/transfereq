class Axis(object):
    """class of Axis on line segment [A,B] with n segments"""   

    def __init__ (self, A, B, n):
        self.A = A
        self.B = B
        self.n = n


    def get_h(self):
        return (self.B - self.A) / (self.n-1)


    def get_point(self,i):
        return self.A + i * (self.B - self.A) / (self.n-1)